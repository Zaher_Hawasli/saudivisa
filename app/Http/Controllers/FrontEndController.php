<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guest;
use App\Jobs\SendEmailJob;

class FrontEndController extends Controller
{
    public function index()
    {

        return view('first_page');
    }


    public function show_second_page(Request $request)
    {
        $this->validate($request, [
            'email'=>'required|unique:guests'
        ]);
        session()->put('email',$request->email);
        $details['email'] = $request->email;

        $details['body'] = "<p>Welcome to RS4IT</p>
        <p>We sending this email to you to complete your Saudi VIAS entry and travel requirement
            Please click on link below
            123456789qwererewr.com
            .</p>";

    dispatch(new SendEmailJob($details));
        return view('second_page');
    }

    public function show_third_page()
    {
        return view('third_page');
    }

    public function review_information()
    {
        $guest_id=session()->get('guest_id');
        $guest=Guest::with(['Accommodation','Guest'])->find($guest_id)->first();
        return view('review_information',compact('guest'));
    }
    
    public function Confirm()
    {
        $email=session()->get('email');
        $details['email'] = $email;

        $details['body'] = "<p>complete information content </p>
        <p>
            <p>Well Done </p>
            Your information has been submitted successfully
            You will receive in coming day invitation email with instructions from RS4IT to book your flight.
            See you soon.
        </p>";

            dispatch(new SendEmailJob($details));
        return view('final_page');
    }

}

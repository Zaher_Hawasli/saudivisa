<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use Illuminate\Http\Request;

class AccommodationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    public function Datatable(Guest $guest)
    {
        $Accommodation=$guest->Accommodation;
        return Datatables::of($Accommodation)
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $guest_id=session()->get('guest_id');
        $companion_id=session()->get('companion_id');
        $accommodation=Accommodation::create(array_merge($request->all(),['guest_id'=>$guest_id]));
        if(isset($companion_id))
        $accommodation=Accommodation::create(array_merge($request->all(),['guest_id'=>$companion_id]));
        return 'Guest accommodation stored successfully';
    }

    /**
     * Display the specified resource.
     */
    public function show(Accommodation $accommodation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Accommodation $accommodation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Accommodation $accommodation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Accommodation $accommodation)
    {
        //
    }
}

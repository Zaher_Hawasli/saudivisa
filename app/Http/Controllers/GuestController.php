<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Validator;
use Illuminate\Support\Facades\File;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('CMS.Guest.Guest');
    }


    public function Datatable()
    {
        $guest=Guest::with(['Accommodation','Guest'])->get();
            return Datatables::of($guest)
            ->addColumn('companion_name', function ($item) {
                $name='';
                if(isset($item->Guest))
                $name=$item->Guest->first_name.' '.$item->Guest->last_name;

        return $name;
            })
            ->addColumn('action', function ($item) {
                return '<a href="#">
                <i data-toggle="modal" 
                data-toggle="tooltip" data-target="#edit_modal" data-placement="top" title="edit" 
                class="fa fa-edit text-success pr-2 edit-button" role="button"></i>
                </a>
                <a href="#">
                <i data-toggle="modal" 
                data-toggle="tooltip" data-target="#Accommodation_modal" data-placement="top" title="Accommodation" 
                class="fa fa-home pr-2 Accommodation-button" role="button"></i>
                </a>
                <button class="delete btn">
                <i
               data-toggle="tooltip" data-placement="top" title="delete" 
                class="fa fa-trash text-danger pointer pr-2"> </i>
                </button>';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'personal_photo' => 'image|mimes:jpeg,png,jpg,webp',
            'passport_photo' => 'image|mimes:jpeg,png,jpg,webp',
        ]);

        if(isset($request->companion)){
            $this->validate($request, [
                'companion[personal_photo]' => 'image|mimes:jpeg,png,jpg,webp',
                'companion[passport_photo]' => 'image|mimes:jpeg,png,jpg,webp',
            ]);
        }

        $email=session()->get('email');
            
            $guest_passport_photo_path =Guest::StorePhoto($request,'personal_photo');

            $guest_personal_photo_path =Guest::StorePhoto($request,'passport_photo');

            $guest=Guest::create(array_merge(
                $request->all(),['personal_photo'=>$guest_personal_photo_path,
                'passport_photo'=>$guest_passport_photo_path,
                'email'=>$email
                ]));

            if(isset($request->companion)){
            $companion_personal_photo_path =Guest::StorePhoto($request,'personal_photo',true);

            $companion_passport_photo_path =Guest::StorePhoto($request,'passport_photo',true);

            $companion=Guest::create(array_merge(
                $request['companion'],['personal_photo'=>$companion_personal_photo_path,
                'passport_photo'=>$companion_passport_photo_path,'companion_for_id'=>$guest->id]));

                // set companion id for guest
                $guest->update(['companion_for_id'=>$companion->id]);
            }

            session()->put('guest_id', $guest->id);
            session()->put('companion_id', $companion->id);

        return 'Guest stored successfully';
        
    }

    /**
     * Display the specified resource.
     */
    public function show(Guest $guest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Guest $guest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Guest $guest)
    {
        $this->validate($request, [
            'personal_photo' => 'image|mimes:jpeg,png,jpg,webp',
            'passport_photo' => 'image|mimes:jpeg,png,jpg,webp',
        ]);
        $updated_photo=[];
            if($request->file('personal_photo')){
                $path=$guest->personal_photo;
                File::delete(public_path($path));
                $guest_personal_photo_path =Guest::StorePhoto($request,'personal_photo');
                array_push($updated_photo,['personal_photo'=>$guest_personal_photo_path,]);
            }

            if($request->file('passport_photo')){
                $path=$guest->passport_photo;
                File::delete(public_path($path));
                $guest_passport_photo_path =Guest::StorePhoto($request,'passport_photo');
                array_push($updated_photo,['passport_photo'=>$guest_passport_photo_path,]);
            }

            $guest->update(array_merge($request->all(), $updated_photo));

                return 'Guest updated successfully';
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Guest $guest)
    {
        $path=$guest->personal_photo;
        File::delete(public_path($path));
        $path=$guest->passport_photo;
        File::delete(public_path($path));

        $guest->delete();
        return 'Guest Deleted successfully';
                
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    use HasFactory;

    protected $fillable=[
        'guest_id',
        'accommodations_type',
        'check_in_date',
        'check_out_date',
        'room_type',
    ];
}

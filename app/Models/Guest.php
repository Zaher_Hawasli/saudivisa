<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;

    protected $fillable=[
        'first_name',
        'last_name',
        'date_of_birth',
        'email',
        'issue_date',
        'expiry_date',
        'arrival_date',
        'place_of_birth',
        'place_of_issue',
        'country_of_residency',
        'gender',
        'passport_no',
        'profession',
        'organization',
        'visa_duration',
        'visa_status',
        'passport_photo',
        'personal_photo',
        'companion_for_id',
    ];

    public function Accommodation()
    {
        return $this->hasMany(Accommodation::class);
    }

    public function Guest()
    {
        return $this->belongsTo(Guest::class,'companion_for_id');
    }


    // move Photo to Photos folder and if not exist will be created by mkdir function
    public static function StorePhoto($request,$photo_type,$is_companion=false){
        if($is_companion){
        $file = $request->file('companion')[$photo_type];
        $name=$request['companion']['first_name'].'_'.$request['companion']['last_name'].'_'.$photo_type.'.'.$file->getClientOriginalExtension();
        }
        else{
        $file = $request->file($photo_type);
        $name=$request->first_name.'_'.$request->last_name.'_'.$photo_type.'.'.$file->getClientOriginalExtension();
        }
        
            if(!is_dir(public_path('Photos')))
            mkdir(public_path('Photos'), 0755);
            $dest = public_path('Photos');
            $file->move($dest, $name);
            $file_path ='Photos/'. $name;
            return $file_path;
    }

}

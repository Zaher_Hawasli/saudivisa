const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

// ****************************************************


let myvar=(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
        }
    });

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    var csrf = $('meta[name=csrf-token]').attr('content');
    var addForm = $('#add-form');
    var editForm = $('#edit-form');
    var add_accommodation_form = $('#add_accommodation_form');
    var save_form = $('#save-form');
    var id = 0;

    $('.select2').select2({
        theme: "classic",
    });

    // $('#add_modal').on('show.bs.modal', function (event) {

    //     $('.validation-errors').html('');
    //     document.getElementById('add-form').reset();

    // });



    addForm.validate({
        errorClass: "error fail-alert",
        validClass: "valid success-alert",
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            issue_date: {
                required: true,
            },
            expiry_date: {
                required: true,
            },
            arrival_date: {
                required: true,
            },
            place_of_birth: {
                required: true,
            },
            date_of_birth: {
                required: true,
            },
            place_of_issue: {
                required: true,
            },
            country_of_residency: {
                required: true,
            },
            gender: {
                required: true,
            },
            passport_no: {
                required: true,
            },
            visa_duration: {
                required: true,
            },
            visa_status: {
                required: true,
            },
            passport_photo: {
                required: true,
            },
            personal_photo: {
                required: true,
            },
            companion_for_id: {
                required: true,
            },
            'companion["first_name"]': {
                required: true,
            },
            'companion["last_name"]': {
                required: true,
            },
            'companion[issue_date]': {
                required: true,
            },
            'companion[expiry_date]': {
                required: true,
            },
            'companion[arrival_date]': {
                required: true,
            },
            'companion[place_of_birth]': {
                required: true,
            },
            'companion[date_of_birth]': {
                required: true,
            },
            'companion[place_of_issue]': {
                required: true,
            },
            'companion[country_of_residency]': {
                required: true,
            },
            'companion[gender]': {
                required: true,
            },
            'companion[passport_no]': {
                required: true,
            },
            'companion[visa_duration]': {
                required: true,
            },
            'companion[visa_status]': {
                required: true,
            },
            'companion[passport_photo]': {
                required: true,
            },
            'companion[personal_photo]': {
                required: true,
            },
            'companion[companion_for_id]': {
                required: true,
            },
        },
        errorElement: 'span',
        submitHandler: function (form) {
            // e.preventDefault();
            var formData = new FormData(form);
            $.ajax({
                'headers': {
                    'X-CSRF-TOKEN': csrf
                },
                url: '/guest',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#next_btn').removeClass('disabled');
                    $('button[type="submit"]').addClass('disabled');
                    Toast.fire({
                        icon: 'success',
                        title: data
                    })
                    table.draw();
                },
                error: function (error) {
                    console.log(error)
                    Toast.fire({
                        icon: 'error',
                        title: error.responseJSON.message
                    })
                },
            })
        }
    });

    editForm.validate({
        errorClass: "error fail-alert",
        errorElement: 'span',
        validClass: "valid success-alert",
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            issue_date: {
                required: true,
            },
            expiry_date: {
                required: true,
            },
            arrival_date: {
                required: true,
            },
            place_of_birth: {
                required: true,
            },
            date_of_birth: {
                required: true,
            },
            place_of_issue: {
                required: true,
            },
            country_of_residency: {
                required: true,
            },
            gender: {
                required: true,
            },
            passport_no: {
                required: true,
            },
            visa_duration: {
                required: true,
            },
            visa_status: {
                required: true,
            },
        },
        submitHandler: function (form) {
            // e.preventDefault();
            var formData = new FormData(form);
            $.ajax({
                'headers': {
                    'X-CSRF-TOKEN': csrf
                },
                url: 'guest/' + id,
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    Toast.fire({
                        icon: 'success',
                        title: data
                    })
                    table.draw();
                    $("#edit_modal").modal("hide")
                },
                error: function (error) {
                    console.log(error)
                    Toast.fire({
                        icon: 'error',
                        title: error.responseJSON.message
                    })
                },
            })
        }
    });



    $('#table tbody').on('click', '.delete', function () {

        var data = (table.row($(this).parents('tr')).data());
        id = data.id;

        Swal.fire({
            title: 'are you sure',
            icon: 'question',
            iconHtml: '؟',
            confirmButtonText: 'Yes',
            cancelButtonText: 'cancel',
            confirmButtonColor: '#d33',
            showCancelButton: true,
            showCloseButton: true
        }).then((result) => {

            if (result.isConfirmed) {

                $.ajax({
                    type: 'delete',
                    url: 'guest/' + id,
                    'headers': {
                        'X-CSRF-TOKEN': csrf
                    },
                    success: function (data) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: data,
                            showConfirmButton: false,
                            confirmButtonColor: '#3085d6',
                            timer: 3300,
                            timerProgressBar: true,
                        })
                        table.draw();
                    }
                })
            }

        })

    });


    $('#table tbody').on('click', '.edit-button', function () {
        var data = (table.row($(this).parents('tr')).data());
        id = data.id;
        for (const [key, value] of Object.entries(data)) {
            if(key!='passport_photo'&&key!='personal_photo')
            editForm.find('input[name="' + key + '"]').val(value);
            editForm.find('select[name="' + key + '"]').val(value);
            editForm.find('select[name="' + key + '"]').trigger('change');
        }

        $('#edit-modal').modal('show');
    });

    $('#table tbody').on('click', '.Accommodation-button', function () {
        var data = (table.row($(this).parents('tr')).data());
        id = data.id;
        console.log(data);
        data.accommodation.forEach((el)=>{
            console.log(el);
            $('#Accommodation_table tbody').append(`
                <td>${el.accommodations_type}</td>
            <td>${el.room_type}</td>
            <td>${el.check_in_date}</td>
            <td>${el.check_out_date}</td>
            `);
        })
        // $('#Accommodation_table tbody').append(
        //     `
        //     <td>${data.Accommodation.accommodations_type}</td>
        //     <td>${data.Accommodation.room_type}</td>
        //     <td>${data.Accommodation.check_in_date}</td>
        //     <td>${data.Accommodation.check_out_date}</td>
        //     `
        //     );

        $('#Accommodation-modal').modal('show');
    });




    // #################### get data ####################

    var table = $('#table').DataTable({
        "columnDefs": [
            { "className": "dt-center", "targets": "_all" }
        ]
        ,
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            type: 'post',
            url: '/GuestDataTable',
        },
        // iDisplayLength: -1,

        columns: [
            {
                data: function (data) {
                    img = `<a href="` + data.personal_photo + `" data-lightbox="` + data.personal_photo + `" data-title="My caption">
                    <img  width="45px" height="45px" class="rounded" data-lity style="cursor: pointer" 
                    src="` + data.personal_photo + `" alt="Img"></a>`
                    return img;
                }
                , name: 'personal_photo'
            },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'date_of_birth', name: 'date_of_birth' },
            { data: 'gender', name: 'gender' },
            { data: 'passport_no', name: 'passport_no' },
            { data: 'visa_status', name: 'visa_status' },
            { data: 'companion_name', name: 'companion_name' },
            {
                data: function (data) {
                    return data.action;
                },
                name: 'action'
            },
        ]
    })

    var table = $('#table').DataTable();




    add_accommodation_form.on('submit', function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            'headers': {
                'X-CSRF-TOKEN': csrf
            },
            url: '/accommodation',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $('#next_btn').removeClass('disabled');
                $('button[type="submit"]').addClass('disabled');
                Toast.fire({
                    icon: 'success',
                    title: data
                })
                table.draw();
            },
            error: function (error) {
                console.log(error)
                Toast.fire({
                    icon: 'error',
                    title: error.responseJSON.message
                })
            },
        });
    });

}());






$('#has_companion').on('change', function () {
    $('#has_companion').attr('checked', '');
    if ($('#has_companion').is(':checked')) {
        let my_date = $('#date_of_birth').attr('max');
        $('#companion_info').html(`<div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Add Companion Info</h1>
        </div>
        <div class="row mt-4">
            <div class="col-6">
               <label for="first_name">First Name</label>
               <input type="text" class="form-control" required pattern="[A-Za-z]{1,15}" id="companion_first_name" name="companion[first_name]">
            </div>
            <div class="col-6">
               <label for="last_name">Last Name</label>
               <input type="text" class="form-control" required pattern="[A-Za-z]{1,15}" id="companion_last_name" name="companion[last_name]">
            </div>
        </div>
        <div class="row mt-4">
           <div class="col-6">
               <label for="date_of_birth">Date of Birth</label>
               <input type="date" class="form-control" required max="${my_date}" id="companion_date_of_birth" name="companion[date_of_birth]">
            </div>
            <div class="col-6">
               <label for="gender">Gender</label>
               <select class="form-control" name="companion[gender]" id="gender">
                   <option disabled selected>Gender</option>
                   <option value="Male">Male</option>
                   <option value="Female">Female</option>
               </select>
            </div>
        </div>
        <div class="row mt-4">
           <div class="col-6">
               <label for="place_of_birth">place of birth</label>
               <select class="select2 form-control" id="companion_place_of_birth" name="companion[place_of_birth]">
                   <option disabled selected>place of birth</option>
                   <option value="Afghanistan">Afghanistan</option>
                   <option value="Aland Islands">Åland Islands</option>
                   <option value="Albania">Albania</option>
                   <option value="Algeria">Algeria</option>
                   <option value="American Samoa">American Samoa</option>
                   <option value="Andorra">Andorra</option>
                   <option value="Angola">Angola</option>
                   <option value="Anguilla">Anguilla</option>
                   <option value="Antarctica">Antarctica</option>
                   <option value="Antigua and Barbuda">Antigua & Barbuda</option>
                   <option value="Argentina">Argentina</option>
                   <option value="Armenia">Armenia</option>
                   <option value="Aruba">Aruba</option>
                   <option value="Australia">Australia</option>
                   <option value="Austria">Austria</option>
                   <option value="Azerbaijan">Azerbaijan</option>
                   <option value="Bahamas">Bahamas</option>
                   <option value="Bahrain">Bahrain</option>
                   <option value="Bangladesh">Bangladesh</option>
                   <option value="Barbados">Barbados</option>
                   <option value="Belarus">Belarus</option>
                   <option value="Belgium">Belgium</option>
                   <option value="Belize">Belize</option>
                   <option value="Benin">Benin</option>
                   <option value="Bermuda">Bermuda</option>
                   <option value="Bhutan">Bhutan</option>
                   <option value="Bolivia">Bolivia</option>
                   <option value="Bonaire, Sint Eustatius and Saba">Caribbean Netherlands</option>
                   <option value="Bosnia and Herzegovina">Bosnia & Herzegovina</option>
                   <option value="Botswana">Botswana</option>
                   <option value="Bouvet Island">Bouvet Island</option>
                   <option value="Brazil">Brazil</option>
                   <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                   <option value="Brunei Darussalam">Brunei</option>
                   <option value="Bulgaria">Bulgaria</option>
                   <option value="Burkina Faso">Burkina Faso</option>
                   <option value="Burundi">Burundi</option>
                   <option value="Cambodia">Cambodia</option>
                   <option value="Cameroon">Cameroon</option>
                   <option value="Canada">Canada</option>
                   <option value="Cape Verde">Cape Verde</option>
                   <option value="Cayman Islands">Cayman Islands</option>
                   <option value="Central African Republic">Central African Republic</option>
                   <option value="Chad">Chad</option>
                   <option value="Chile">Chile</option>
                   <option value="China">China</option>
                   <option value="Christmas Island">Christmas Island</option>
                   <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                   <option value="Colombia">Colombia</option>
                   <option value="Comoros">Comoros</option>
                   <option value="Congo">Congo - Brazzaville</option>
                   <option value="Congo, Democratic Republic of the Congo">Congo - Kinshasa</option>
                   <option value="Cook Islands">Cook Islands</option>
                   <option value="Costa Rica">Costa Rica</option>
                   <option value="Cote D'Ivoire">Côte d’Ivoire</option>
                   <option value="Croatia">Croatia</option>
                   <option value="Cuba">Cuba</option>
                   <option value="Curacao">Curaçao</option>
                   <option value="Cyprus">Cyprus</option>
                   <option value="Czech Republic">Czechia</option>
                   <option value="Denmark">Denmark</option>
                   <option value="Djibouti">Djibouti</option>
                   <option value="Dominica">Dominica</option>
                   <option value="Dominican Republic">Dominican Republic</option>
                   <option value="Ecuador">Ecuador</option>
                   <option value="Egypt">Egypt</option>
                   <option value="El Salvador">El Salvador</option>
                   <option value="Equatorial Guinea">Equatorial Guinea</option>
                   <option value="Eritrea">Eritrea</option>
                   <option value="Estonia">Estonia</option>
                   <option value="Ethiopia">Ethiopia</option>
                   <option value="Falkland Islands (Malvinas)">Falkland Islands (Islas Malvinas)</option>
                   <option value="Faroe Islands">Faroe Islands</option>
                   <option value="Fiji">Fiji</option>
                   <option value="Finland">Finland</option>
                   <option value="France">France</option>
                   <option value="French Guiana">French Guiana</option>
                   <option value="French Polynesia">French Polynesia</option>
                   <option value="French Southern Territories">French Southern Territories</option>
                   <option value="Gabon">Gabon</option>
                   <option value="Gambia">Gambia</option>
                   <option value="Georgia">Georgia</option>
                   <option value="Germany">Germany</option>
                   <option value="Ghana">Ghana</option>
                   <option value="Gibraltar">Gibraltar</option>
                   <option value="Greece">Greece</option>
                   <option value="Greenland">Greenland</option>
                   <option value="Grenada">Grenada</option>
                   <option value="Guadeloupe">Guadeloupe</option>
                   <option value="Guam">Guam</option>
                   <option value="Guatemala">Guatemala</option>
                   <option value="Guernsey">Guernsey</option>
                   <option value="Guinea">Guinea</option>
                   <option value="Guinea-Bissau">Guinea-Bissau</option>
                   <option value="Guyana">Guyana</option>
                   <option value="Haiti">Haiti</option>
                   <option value="Heard Island and Mcdonald Islands">Heard & McDonald Islands</option>
                   <option value="Holy See (Vatican City State)">Vatican City</option>
                   <option value="Honduras">Honduras</option>
                   <option value="Hong Kong">Hong Kong</option>
                   <option value="Hungary">Hungary</option>
                   <option value="Iceland">Iceland</option>
                   <option value="India">India</option>
                   <option value="Indonesia">Indonesia</option>
                   <option value="Iran, Islamic Republic of">Iran</option>
                   <option value="Iraq">Iraq</option>
                   <option value="Ireland">Ireland</option>
                   <option value="Isle of Man">Isle of Man</option>
                   <option value="Israel">Israel</option>
                   <option value="Italy">Italy</option>
                   <option value="Jamaica">Jamaica</option>
                   <option value="Japan">Japan</option>
                   <option value="Jersey">Jersey</option>
                   <option value="Jordan">Jordan</option>
                   <option value="Kazakhstan">Kazakhstan</option>
                   <option value="Kenya">Kenya</option>
                   <option value="Kiribati">Kiribati</option>
                   <option value="Korea, Democratic People's Republic of">North Korea</option>
                   <option value="Korea, Republic of">South Korea</option>
                   <option value="Kosovo">Kosovo</option>
                   <option value="Kuwait">Kuwait</option>
                   <option value="Kyrgyzstan">Kyrgyzstan</option>
                   <option value="Lao People's Democratic Republic">Laos</option>
                   <option value="Latvia">Latvia</option>
                   <option value="Lebanon">Lebanon</option>
                   <option value="Lesotho">Lesotho</option>
                   <option value="Liberia">Liberia</option>
                   <option value="Libyan Arab Jamahiriya">Libya</option>
                   <option value="Liechtenstein">Liechtenstein</option>
                   <option value="Lithuania">Lithuania</option>
                   <option value="Luxembourg">Luxembourg</option>
                   <option value="Macao">Macao</option>
                   <option value="Macedonia, the Former Yugoslav Republic of">North Macedonia</option>
                   <option value="Madagascar">Madagascar</option>
                   <option value="Malawi">Malawi</option>
                   <option value="Malaysia">Malaysia</option>
                   <option value="Maldives">Maldives</option>
                   <option value="Mali">Mali</option>
                   <option value="Malta">Malta</option>
                   <option value="Marshall Islands">Marshall Islands</option>
                   <option value="Martinique">Martinique</option>
                   <option value="Mauritania">Mauritania</option>
                   <option value="Mauritius">Mauritius</option>
                   <option value="Mayotte">Mayotte</option>
                   <option value="Mexico">Mexico</option>
                   <option value="Micronesia, Federated States of">Micronesia</option>
                   <option value="Moldova, Republic of">Moldova</option>
                   <option value="Monaco">Monaco</option>
                   <option value="Mongolia">Mongolia</option>
                   <option value="Montenegro">Montenegro</option>
                   <option value="Montserrat">Montserrat</option>
                   <option value="Morocco">Morocco</option>
                   <option value="Mozambique">Mozambique</option>
                   <option value="Myanmar">Myanmar (Burma)</option>
                   <option value="Namibia">Namibia</option>
                   <option value="Nauru">Nauru</option>
                   <option value="Nepal">Nepal</option>
                   <option value="Netherlands">Netherlands</option>
                   <option value="Netherlands Antilles">Curaçao</option>
                   <option value="New Caledonia">New Caledonia</option>
                   <option value="New Zealand">New Zealand</option>
                   <option value="Nicaragua">Nicaragua</option>
                   <option value="Niger">Niger</option>
                   <option value="Nigeria">Nigeria</option>
                   <option value="Niue">Niue</option>
                   <option value="Norfolk Island">Norfolk Island</option>
                   <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                   <option value="Norway">Norway</option>
                   <option value="Oman">Oman</option>
                   <option value="Pakistan">Pakistan</option>
                   <option value="Palau">Palau</option>
                   <option value="Palestinian Territory, Occupied">Palestine</option>
                   <option value="Panama">Panama</option>
                   <option value="Papua New Guinea">Papua New Guinea</option>
                   <option value="Paraguay">Paraguay</option>
                   <option value="Peru">Peru</option>
                   <option value="Philippines">Philippines</option>
                   <option value="Pitcairn">Pitcairn Islands</option>
                   <option value="Poland">Poland</option>
                   <option value="Portugal">Portugal</option>
                   <option value="Puerto Rico">Puerto Rico</option>
                   <option value="Qatar">Qatar</option>
                   <option value="Reunion">Réunion</option>
                   <option value="Romania">Romania</option>
                   <option value="Russian Federation">Russia</option>
                   <option value="Rwanda">Rwanda</option>
                   <option value="Saint Barthelemy">St. Barthélemy</option>
                   <option value="Saint Helena">St. Helena</option>
                   <option value="Saint Kitts and Nevis">St. Kitts & Nevis</option>
                   <option value="Saint Lucia">St. Lucia</option>
                   <option value="Saint Martin">St. Martin</option>
                   <option value="Saint Pierre and Miquelon">St. Pierre & Miquelon</option>
                   <option value="Saint Vincent and the Grenadines">St. Vincent & Grenadines</option>
                   <option value="Samoa">Samoa</option>
                   <option value="San Marino">San Marino</option>
                   <option value="Sao Tome and Principe">São Tomé & Príncipe</option>
                   <option value="Saudi Arabia">Saudi Arabia</option>
                   <option value="Senegal">Senegal</option>
                   <option value="Serbia">Serbia</option>
                   <option value="Serbia and Montenegro">Serbia</option>
                   <option value="Seychelles">Seychelles</option>
                   <option value="Sierra Leone">Sierra Leone</option>
                   <option value="Singapore">Singapore</option>
                   <option value="Sint Maarten">Sint Maarten</option>
                   <option value="Slovakia">Slovakia</option>
                   <option value="Slovenia">Slovenia</option>
                   <option value="Solomon Islands">Solomon Islands</option>
                   <option value="Somalia">Somalia</option>
                   <option value="South Africa">South Africa</option>
                   <option value="South Georgia and the South Sandwich Islands">South Georgia & South Sandwich Islands</option>
                   <option value="South Sudan">South Sudan</option>
                   <option value="Spain">Spain</option>
                   <option value="Sri Lanka">Sri Lanka</option>
                   <option value="Sudan">Sudan</option>
                   <option value="Suriname">Suriname</option>
                   <option value="Svalbard and Jan Mayen">Svalbard & Jan Mayen</option>
                   <option value="Swaziland">Eswatini</option>
                   <option value="Sweden">Sweden</option>
                   <option value="Switzerland">Switzerland</option>
                   <option value="Syrian Arab Republic">Syria</option>
                   <option value="Taiwan, Province of China">Taiwan</option>
                   <option value="Tajikistan">Tajikistan</option>
                   <option value="Tanzania, United Republic of">Tanzania</option>
                   <option value="Thailand">Thailand</option>
                   <option value="Timor-Leste">Timor-Leste</option>
                   <option value="Togo">Togo</option>
                   <option value="Tokelau">Tokelau</option>
                   <option value="Tonga">Tonga</option>
                   <option value="Trinidad and Tobago">Trinidad & Tobago</option>
                   <option value="Tunisia">Tunisia</option>
                   <option value="Turkey">Turkey</option>
                   <option value="Turkmenistan">Turkmenistan</option>
                   <option value="Turks and Caicos Islands">Turks & Caicos Islands</option>
                   <option value="Tuvalu">Tuvalu</option>
                   <option value="Uganda">Uganda</option>
                   <option value="Ukraine">Ukraine</option>
                   <option value="United Arab Emirates">United Arab Emirates</option>
                   <option value="United Kingdom">United Kingdom</option>
                   <option value="United States">United States</option>
                   <option value="United States Minor Outlying Islands">U.S. Outlying Islands</option>
                   <option value="Uruguay">Uruguay</option>
                   <option value="Uzbekistan">Uzbekistan</option>
                   <option value="Vanuatu">Vanuatu</option>
                   <option value="Venezuela">Venezuela</option>
                   <option value="Viet Nam">Vietnam</option>
                   <option value="Virgin Islands, British">British Virgin Islands</option>
                   <option value="Virgin Islands, U.s.">U.S. Virgin Islands</option>
                   <option value="Wallis and Futuna">Wallis & Futuna</option>
                   <option value="Western Sahara">Western Sahara</option>
                   <option value="Yemen">Yemen</option>
                   <option value="Zambia">Zambia</option>
                   <option value="Zimbabwe">Zimbabwe</option>
               </select>
            </div>
            <div class="col-6">
               <label for="country_of_residency">Country of residency</label>
               <select class="select2 form-control" id="companion_country_of_residency" name="companion[country_of_residency]">
                   <option disabled selected>Country of residency</option>
                   <option value="Afghanistan">Afghanistan</option>
                   <option value="Aland Islands">Åland Islands</option>
                   <option value="Albania">Albania</option>
                   <option value="Algeria">Algeria</option>
                   <option value="American Samoa">American Samoa</option>
                   <option value="Andorra">Andorra</option>
                   <option value="Angola">Angola</option>
                   <option value="Anguilla">Anguilla</option>
                   <option value="Antarctica">Antarctica</option>
                   <option value="Antigua and Barbuda">Antigua & Barbuda</option>
                   <option value="Argentina">Argentina</option>
                   <option value="Armenia">Armenia</option>
                   <option value="Aruba">Aruba</option>
                   <option value="Australia">Australia</option>
                   <option value="Austria">Austria</option>
                   <option value="Azerbaijan">Azerbaijan</option>
                   <option value="Bahamas">Bahamas</option>
                   <option value="Bahrain">Bahrain</option>
                   <option value="Bangladesh">Bangladesh</option>
                   <option value="Barbados">Barbados</option>
                   <option value="Belarus">Belarus</option>
                   <option value="Belgium">Belgium</option>
                   <option value="Belize">Belize</option>
                   <option value="Benin">Benin</option>
                   <option value="Bermuda">Bermuda</option>
                   <option value="Bhutan">Bhutan</option>
                   <option value="Bolivia">Bolivia</option>
                   <option value="Bonaire, Sint Eustatius and Saba">Caribbean Netherlands</option>
                   <option value="Bosnia and Herzegovina">Bosnia & Herzegovina</option>
                   <option value="Botswana">Botswana</option>
                   <option value="Bouvet Island">Bouvet Island</option>
                   <option value="Brazil">Brazil</option>
                   <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                   <option value="Brunei Darussalam">Brunei</option>
                   <option value="Bulgaria">Bulgaria</option>
                   <option value="Burkina Faso">Burkina Faso</option>
                   <option value="Burundi">Burundi</option>
                   <option value="Cambodia">Cambodia</option>
                   <option value="Cameroon">Cameroon</option>
                   <option value="Canada">Canada</option>
                   <option value="Cape Verde">Cape Verde</option>
                   <option value="Cayman Islands">Cayman Islands</option>
                   <option value="Central African Republic">Central African Republic</option>
                   <option value="Chad">Chad</option>
                   <option value="Chile">Chile</option>
                   <option value="China">China</option>
                   <option value="Christmas Island">Christmas Island</option>
                   <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                   <option value="Colombia">Colombia</option>
                   <option value="Comoros">Comoros</option>
                   <option value="Congo">Congo - Brazzaville</option>
                   <option value="Congo, Democratic Republic of the Congo">Congo - Kinshasa</option>
                   <option value="Cook Islands">Cook Islands</option>
                   <option value="Costa Rica">Costa Rica</option>
                   <option value="Cote D'Ivoire">Côte d’Ivoire</option>
                   <option value="Croatia">Croatia</option>
                   <option value="Cuba">Cuba</option>
                   <option value="Curacao">Curaçao</option>
                   <option value="Cyprus">Cyprus</option>
                   <option value="Czech Republic">Czechia</option>
                   <option value="Denmark">Denmark</option>
                   <option value="Djibouti">Djibouti</option>
                   <option value="Dominica">Dominica</option>
                   <option value="Dominican Republic">Dominican Republic</option>
                   <option value="Ecuador">Ecuador</option>
                   <option value="Egypt">Egypt</option>
                   <option value="El Salvador">El Salvador</option>
                   <option value="Equatorial Guinea">Equatorial Guinea</option>
                   <option value="Eritrea">Eritrea</option>
                   <option value="Estonia">Estonia</option>
                   <option value="Ethiopia">Ethiopia</option>
                   <option value="Falkland Islands (Malvinas)">Falkland Islands (Islas Malvinas)</option>
                   <option value="Faroe Islands">Faroe Islands</option>
                   <option value="Fiji">Fiji</option>
                   <option value="Finland">Finland</option>
                   <option value="France">France</option>
                   <option value="French Guiana">French Guiana</option>
                   <option value="French Polynesia">French Polynesia</option>
                   <option value="French Southern Territories">French Southern Territories</option>
                   <option value="Gabon">Gabon</option>
                   <option value="Gambia">Gambia</option>
                   <option value="Georgia">Georgia</option>
                   <option value="Germany">Germany</option>
                   <option value="Ghana">Ghana</option>
                   <option value="Gibraltar">Gibraltar</option>
                   <option value="Greece">Greece</option>
                   <option value="Greenland">Greenland</option>
                   <option value="Grenada">Grenada</option>
                   <option value="Guadeloupe">Guadeloupe</option>
                   <option value="Guam">Guam</option>
                   <option value="Guatemala">Guatemala</option>
                   <option value="Guernsey">Guernsey</option>
                   <option value="Guinea">Guinea</option>
                   <option value="Guinea-Bissau">Guinea-Bissau</option>
                   <option value="Guyana">Guyana</option>
                   <option value="Haiti">Haiti</option>
                   <option value="Heard Island and Mcdonald Islands">Heard & McDonald Islands</option>
                   <option value="Holy See (Vatican City State)">Vatican City</option>
                   <option value="Honduras">Honduras</option>
                   <option value="Hong Kong">Hong Kong</option>
                   <option value="Hungary">Hungary</option>
                   <option value="Iceland">Iceland</option>
                   <option value="India">India</option>
                   <option value="Indonesia">Indonesia</option>
                   <option value="Iran, Islamic Republic of">Iran</option>
                   <option value="Iraq">Iraq</option>
                   <option value="Ireland">Ireland</option>
                   <option value="Isle of Man">Isle of Man</option>
                   <option value="Israel">Israel</option>
                   <option value="Italy">Italy</option>
                   <option value="Jamaica">Jamaica</option>
                   <option value="Japan">Japan</option>
                   <option value="Jersey">Jersey</option>
                   <option value="Jordan">Jordan</option>
                   <option value="Kazakhstan">Kazakhstan</option>
                   <option value="Kenya">Kenya</option>
                   <option value="Kiribati">Kiribati</option>
                   <option value="Korea, Democratic People's Republic of">North Korea</option>
                   <option value="Korea, Republic of">South Korea</option>
                   <option value="Kosovo">Kosovo</option>
                   <option value="Kuwait">Kuwait</option>
                   <option value="Kyrgyzstan">Kyrgyzstan</option>
                   <option value="Lao People's Democratic Republic">Laos</option>
                   <option value="Latvia">Latvia</option>
                   <option value="Lebanon">Lebanon</option>
                   <option value="Lesotho">Lesotho</option>
                   <option value="Liberia">Liberia</option>
                   <option value="Libyan Arab Jamahiriya">Libya</option>
                   <option value="Liechtenstein">Liechtenstein</option>
                   <option value="Lithuania">Lithuania</option>
                   <option value="Luxembourg">Luxembourg</option>
                   <option value="Macao">Macao</option>
                   <option value="Macedonia, the Former Yugoslav Republic of">North Macedonia</option>
                   <option value="Madagascar">Madagascar</option>
                   <option value="Malawi">Malawi</option>
                   <option value="Malaysia">Malaysia</option>
                   <option value="Maldives">Maldives</option>
                   <option value="Mali">Mali</option>
                   <option value="Malta">Malta</option>
                   <option value="Marshall Islands">Marshall Islands</option>
                   <option value="Martinique">Martinique</option>
                   <option value="Mauritania">Mauritania</option>
                   <option value="Mauritius">Mauritius</option>
                   <option value="Mayotte">Mayotte</option>
                   <option value="Mexico">Mexico</option>
                   <option value="Micronesia, Federated States of">Micronesia</option>
                   <option value="Moldova, Republic of">Moldova</option>
                   <option value="Monaco">Monaco</option>
                   <option value="Mongolia">Mongolia</option>
                   <option value="Montenegro">Montenegro</option>
                   <option value="Montserrat">Montserrat</option>
                   <option value="Morocco">Morocco</option>
                   <option value="Mozambique">Mozambique</option>
                   <option value="Myanmar">Myanmar (Burma)</option>
                   <option value="Namibia">Namibia</option>
                   <option value="Nauru">Nauru</option>
                   <option value="Nepal">Nepal</option>
                   <option value="Netherlands">Netherlands</option>
                   <option value="Netherlands Antilles">Curaçao</option>
                   <option value="New Caledonia">New Caledonia</option>
                   <option value="New Zealand">New Zealand</option>
                   <option value="Nicaragua">Nicaragua</option>
                   <option value="Niger">Niger</option>
                   <option value="Nigeria">Nigeria</option>
                   <option value="Niue">Niue</option>
                   <option value="Norfolk Island">Norfolk Island</option>
                   <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                   <option value="Norway">Norway</option>
                   <option value="Oman">Oman</option>
                   <option value="Pakistan">Pakistan</option>
                   <option value="Palau">Palau</option>
                   <option value="Palestinian Territory, Occupied">Palestine</option>
                   <option value="Panama">Panama</option>
                   <option value="Papua New Guinea">Papua New Guinea</option>
                   <option value="Paraguay">Paraguay</option>
                   <option value="Peru">Peru</option>
                   <option value="Philippines">Philippines</option>
                   <option value="Pitcairn">Pitcairn Islands</option>
                   <option value="Poland">Poland</option>
                   <option value="Portugal">Portugal</option>
                   <option value="Puerto Rico">Puerto Rico</option>
                   <option value="Qatar">Qatar</option>
                   <option value="Reunion">Réunion</option>
                   <option value="Romania">Romania</option>
                   <option value="Russian Federation">Russia</option>
                   <option value="Rwanda">Rwanda</option>
                   <option value="Saint Barthelemy">St. Barthélemy</option>
                   <option value="Saint Helena">St. Helena</option>
                   <option value="Saint Kitts and Nevis">St. Kitts & Nevis</option>
                   <option value="Saint Lucia">St. Lucia</option>
                   <option value="Saint Martin">St. Martin</option>
                   <option value="Saint Pierre and Miquelon">St. Pierre & Miquelon</option>
                   <option value="Saint Vincent and the Grenadines">St. Vincent & Grenadines</option>
                   <option value="Samoa">Samoa</option>
                   <option value="San Marino">San Marino</option>
                   <option value="Sao Tome and Principe">São Tomé & Príncipe</option>
                   <option value="Saudi Arabia">Saudi Arabia</option>
                   <option value="Senegal">Senegal</option>
                   <option value="Serbia">Serbia</option>
                   <option value="Serbia and Montenegro">Serbia</option>
                   <option value="Seychelles">Seychelles</option>
                   <option value="Sierra Leone">Sierra Leone</option>
                   <option value="Singapore">Singapore</option>
                   <option value="Sint Maarten">Sint Maarten</option>
                   <option value="Slovakia">Slovakia</option>
                   <option value="Slovenia">Slovenia</option>
                   <option value="Solomon Islands">Solomon Islands</option>
                   <option value="Somalia">Somalia</option>
                   <option value="South Africa">South Africa</option>
                   <option value="South Georgia and the South Sandwich Islands">South Georgia & South Sandwich Islands</option>
                   <option value="South Sudan">South Sudan</option>
                   <option value="Spain">Spain</option>
                   <option value="Sri Lanka">Sri Lanka</option>
                   <option value="Sudan">Sudan</option>
                   <option value="Suriname">Suriname</option>
                   <option value="Svalbard and Jan Mayen">Svalbard & Jan Mayen</option>
                   <option value="Swaziland">Eswatini</option>
                   <option value="Sweden">Sweden</option>
                   <option value="Switzerland">Switzerland</option>
                   <option value="Syrian Arab Republic">Syria</option>
                   <option value="Taiwan, Province of China">Taiwan</option>
                   <option value="Tajikistan">Tajikistan</option>
                   <option value="Tanzania, United Republic of">Tanzania</option>
                   <option value="Thailand">Thailand</option>
                   <option value="Timor-Leste">Timor-Leste</option>
                   <option value="Togo">Togo</option>
                   <option value="Tokelau">Tokelau</option>
                   <option value="Tonga">Tonga</option>
                   <option value="Trinidad and Tobago">Trinidad & Tobago</option>
                   <option value="Tunisia">Tunisia</option>
                   <option value="Turkey">Turkey</option>
                   <option value="Turkmenistan">Turkmenistan</option>
                   <option value="Turks and Caicos Islands">Turks & Caicos Islands</option>
                   <option value="Tuvalu">Tuvalu</option>
                   <option value="Uganda">Uganda</option>
                   <option value="Ukraine">Ukraine</option>
                   <option value="United Arab Emirates">United Arab Emirates</option>
                   <option value="United Kingdom">United Kingdom</option>
                   <option value="United States">United States</option>
                   <option value="United States Minor Outlying Islands">U.S. Outlying Islands</option>
                   <option value="Uruguay">Uruguay</option>
                   <option value="Uzbekistan">Uzbekistan</option>
                   <option value="Vanuatu">Vanuatu</option>
                   <option value="Venezuela">Venezuela</option>
                   <option value="Viet Nam">Vietnam</option>
                   <option value="Virgin Islands, British">British Virgin Islands</option>
                   <option value="Virgin Islands, U.s.">U.S. Virgin Islands</option>
                   <option value="Wallis and Futuna">Wallis & Futuna</option>
                   <option value="Western Sahara">Western Sahara</option>
                   <option value="Yemen">Yemen</option>
                   <option value="Zambia">Zambia</option>
                   <option value="Zimbabwe">Zimbabwe</option>
               </select>
            </div>
        </div>
        <div class="row mt-4">
           <div class="col-6">
               <label for="passport_no">Passport No</label>
               <input type="text" class="form-control" id="companion_passport_no" pattern="[A-Za-z0-9]{6,}" name="companion[passport_no]">
            </div>
            <div class="col-6">
               <label for="issue_date">Issue date</label>
               <input type="date" class="form-control" max="${my_date}" id="companion_issue_date" name="companion[issue_date]">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-6">
               <label for="expiry_date">Expiry date</label>
               <input type="date" class="form-control" min="${my_date}" id="companion_expiry_date" name="companion[expiry_date]">
            </div>
            <div class="col-6">
               <label for="place_of_issue">Place of issue</label>
               <select class="select2 form-control" id="companion_place_of_issue" name="companion[place_of_issue]">
                   <option disabled selected>Place of issue</option>
                   <option value="Afghanistan">Afghanistan</option>
                   <option value="Aland Islands">Åland Islands</option>
                   <option value="Albania">Albania</option>
                   <option value="Algeria">Algeria</option>
                   <option value="American Samoa">American Samoa</option>
                   <option value="Andorra">Andorra</option>
                   <option value="Angola">Angola</option>
                   <option value="Anguilla">Anguilla</option>
                   <option value="Antarctica">Antarctica</option>
                   <option value="Antigua and Barbuda">Antigua & Barbuda</option>
                   <option value="Argentina">Argentina</option>
                   <option value="Armenia">Armenia</option>
                   <option value="Aruba">Aruba</option>
                   <option value="Australia">Australia</option>
                   <option value="Austria">Austria</option>
                   <option value="Azerbaijan">Azerbaijan</option>
                   <option value="Bahamas">Bahamas</option>
                   <option value="Bahrain">Bahrain</option>
                   <option value="Bangladesh">Bangladesh</option>
                   <option value="Barbados">Barbados</option>
                   <option value="Belarus">Belarus</option>
                   <option value="Belgium">Belgium</option>
                   <option value="Belize">Belize</option>
                   <option value="Benin">Benin</option>
                   <option value="Bermuda">Bermuda</option>
                   <option value="Bhutan">Bhutan</option>
                   <option value="Bolivia">Bolivia</option>
                   <option value="Bonaire, Sint Eustatius and Saba">Caribbean Netherlands</option>
                   <option value="Bosnia and Herzegovina">Bosnia & Herzegovina</option>
                   <option value="Botswana">Botswana</option>
                   <option value="Bouvet Island">Bouvet Island</option>
                   <option value="Brazil">Brazil</option>
                   <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                   <option value="Brunei Darussalam">Brunei</option>
                   <option value="Bulgaria">Bulgaria</option>
                   <option value="Burkina Faso">Burkina Faso</option>
                   <option value="Burundi">Burundi</option>
                   <option value="Cambodia">Cambodia</option>
                   <option value="Cameroon">Cameroon</option>
                   <option value="Canada">Canada</option>
                   <option value="Cape Verde">Cape Verde</option>
                   <option value="Cayman Islands">Cayman Islands</option>
                   <option value="Central African Republic">Central African Republic</option>
                   <option value="Chad">Chad</option>
                   <option value="Chile">Chile</option>
                   <option value="China">China</option>
                   <option value="Christmas Island">Christmas Island</option>
                   <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                   <option value="Colombia">Colombia</option>
                   <option value="Comoros">Comoros</option>
                   <option value="Congo">Congo - Brazzaville</option>
                   <option value="Congo, Democratic Republic of the Congo">Congo - Kinshasa</option>
                   <option value="Cook Islands">Cook Islands</option>
                   <option value="Costa Rica">Costa Rica</option>
                   <option value="Cote D'Ivoire">Côte d’Ivoire</option>
                   <option value="Croatia">Croatia</option>
                   <option value="Cuba">Cuba</option>
                   <option value="Curacao">Curaçao</option>
                   <option value="Cyprus">Cyprus</option>
                   <option value="Czech Republic">Czechia</option>
                   <option value="Denmark">Denmark</option>
                   <option value="Djibouti">Djibouti</option>
                   <option value="Dominica">Dominica</option>
                   <option value="Dominican Republic">Dominican Republic</option>
                   <option value="Ecuador">Ecuador</option>
                   <option value="Egypt">Egypt</option>
                   <option value="El Salvador">El Salvador</option>
                   <option value="Equatorial Guinea">Equatorial Guinea</option>
                   <option value="Eritrea">Eritrea</option>
                   <option value="Estonia">Estonia</option>
                   <option value="Ethiopia">Ethiopia</option>
                   <option value="Falkland Islands (Malvinas)">Falkland Islands (Islas Malvinas)</option>
                   <option value="Faroe Islands">Faroe Islands</option>
                   <option value="Fiji">Fiji</option>
                   <option value="Finland">Finland</option>
                   <option value="France">France</option>
                   <option value="French Guiana">French Guiana</option>
                   <option value="French Polynesia">French Polynesia</option>
                   <option value="French Southern Territories">French Southern Territories</option>
                   <option value="Gabon">Gabon</option>
                   <option value="Gambia">Gambia</option>
                   <option value="Georgia">Georgia</option>
                   <option value="Germany">Germany</option>
                   <option value="Ghana">Ghana</option>
                   <option value="Gibraltar">Gibraltar</option>
                   <option value="Greece">Greece</option>
                   <option value="Greenland">Greenland</option>
                   <option value="Grenada">Grenada</option>
                   <option value="Guadeloupe">Guadeloupe</option>
                   <option value="Guam">Guam</option>
                   <option value="Guatemala">Guatemala</option>
                   <option value="Guernsey">Guernsey</option>
                   <option value="Guinea">Guinea</option>
                   <option value="Guinea-Bissau">Guinea-Bissau</option>
                   <option value="Guyana">Guyana</option>
                   <option value="Haiti">Haiti</option>
                   <option value="Heard Island and Mcdonald Islands">Heard & McDonald Islands</option>
                   <option value="Holy See (Vatican City State)">Vatican City</option>
                   <option value="Honduras">Honduras</option>
                   <option value="Hong Kong">Hong Kong</option>
                   <option value="Hungary">Hungary</option>
                   <option value="Iceland">Iceland</option>
                   <option value="India">India</option>
                   <option value="Indonesia">Indonesia</option>
                   <option value="Iran, Islamic Republic of">Iran</option>
                   <option value="Iraq">Iraq</option>
                   <option value="Ireland">Ireland</option>
                   <option value="Isle of Man">Isle of Man</option>
                   <option value="Israel">Israel</option>
                   <option value="Italy">Italy</option>
                   <option value="Jamaica">Jamaica</option>
                   <option value="Japan">Japan</option>
                   <option value="Jersey">Jersey</option>
                   <option value="Jordan">Jordan</option>
                   <option value="Kazakhstan">Kazakhstan</option>
                   <option value="Kenya">Kenya</option>
                   <option value="Kiribati">Kiribati</option>
                   <option value="Korea, Democratic People's Republic of">North Korea</option>
                   <option value="Korea, Republic of">South Korea</option>
                   <option value="Kosovo">Kosovo</option>
                   <option value="Kuwait">Kuwait</option>
                   <option value="Kyrgyzstan">Kyrgyzstan</option>
                   <option value="Lao People's Democratic Republic">Laos</option>
                   <option value="Latvia">Latvia</option>
                   <option value="Lebanon">Lebanon</option>
                   <option value="Lesotho">Lesotho</option>
                   <option value="Liberia">Liberia</option>
                   <option value="Libyan Arab Jamahiriya">Libya</option>
                   <option value="Liechtenstein">Liechtenstein</option>
                   <option value="Lithuania">Lithuania</option>
                   <option value="Luxembourg">Luxembourg</option>
                   <option value="Macao">Macao</option>
                   <option value="Macedonia, the Former Yugoslav Republic of">North Macedonia</option>
                   <option value="Madagascar">Madagascar</option>
                   <option value="Malawi">Malawi</option>
                   <option value="Malaysia">Malaysia</option>
                   <option value="Maldives">Maldives</option>
                   <option value="Mali">Mali</option>
                   <option value="Malta">Malta</option>
                   <option value="Marshall Islands">Marshall Islands</option>
                   <option value="Martinique">Martinique</option>
                   <option value="Mauritania">Mauritania</option>
                   <option value="Mauritius">Mauritius</option>
                   <option value="Mayotte">Mayotte</option>
                   <option value="Mexico">Mexico</option>
                   <option value="Micronesia, Federated States of">Micronesia</option>
                   <option value="Moldova, Republic of">Moldova</option>
                   <option value="Monaco">Monaco</option>
                   <option value="Mongolia">Mongolia</option>
                   <option value="Montenegro">Montenegro</option>
                   <option value="Montserrat">Montserrat</option>
                   <option value="Morocco">Morocco</option>
                   <option value="Mozambique">Mozambique</option>
                   <option value="Myanmar">Myanmar (Burma)</option>
                   <option value="Namibia">Namibia</option>
                   <option value="Nauru">Nauru</option>
                   <option value="Nepal">Nepal</option>
                   <option value="Netherlands">Netherlands</option>
                   <option value="Netherlands Antilles">Curaçao</option>
                   <option value="New Caledonia">New Caledonia</option>
                   <option value="New Zealand">New Zealand</option>
                   <option value="Nicaragua">Nicaragua</option>
                   <option value="Niger">Niger</option>
                   <option value="Nigeria">Nigeria</option>
                   <option value="Niue">Niue</option>
                   <option value="Norfolk Island">Norfolk Island</option>
                   <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                   <option value="Norway">Norway</option>
                   <option value="Oman">Oman</option>
                   <option value="Pakistan">Pakistan</option>
                   <option value="Palau">Palau</option>
                   <option value="Palestinian Territory, Occupied">Palestine</option>
                   <option value="Panama">Panama</option>
                   <option value="Papua New Guinea">Papua New Guinea</option>
                   <option value="Paraguay">Paraguay</option>
                   <option value="Peru">Peru</option>
                   <option value="Philippines">Philippines</option>
                   <option value="Pitcairn">Pitcairn Islands</option>
                   <option value="Poland">Poland</option>
                   <option value="Portugal">Portugal</option>
                   <option value="Puerto Rico">Puerto Rico</option>
                   <option value="Qatar">Qatar</option>
                   <option value="Reunion">Réunion</option>
                   <option value="Romania">Romania</option>
                   <option value="Russian Federation">Russia</option>
                   <option value="Rwanda">Rwanda</option>
                   <option value="Saint Barthelemy">St. Barthélemy</option>
                   <option value="Saint Helena">St. Helena</option>
                   <option value="Saint Kitts and Nevis">St. Kitts & Nevis</option>
                   <option value="Saint Lucia">St. Lucia</option>
                   <option value="Saint Martin">St. Martin</option>
                   <option value="Saint Pierre and Miquelon">St. Pierre & Miquelon</option>
                   <option value="Saint Vincent and the Grenadines">St. Vincent & Grenadines</option>
                   <option value="Samoa">Samoa</option>
                   <option value="San Marino">San Marino</option>
                   <option value="Sao Tome and Principe">São Tomé & Príncipe</option>
                   <option value="Saudi Arabia">Saudi Arabia</option>
                   <option value="Senegal">Senegal</option>
                   <option value="Serbia">Serbia</option>
                   <option value="Serbia and Montenegro">Serbia</option>
                   <option value="Seychelles">Seychelles</option>
                   <option value="Sierra Leone">Sierra Leone</option>
                   <option value="Singapore">Singapore</option>
                   <option value="Sint Maarten">Sint Maarten</option>
                   <option value="Slovakia">Slovakia</option>
                   <option value="Slovenia">Slovenia</option>
                   <option value="Solomon Islands">Solomon Islands</option>
                   <option value="Somalia">Somalia</option>
                   <option value="South Africa">South Africa</option>
                   <option value="South Georgia and the South Sandwich Islands">South Georgia & South Sandwich Islands</option>
                   <option value="South Sudan">South Sudan</option>
                   <option value="Spain">Spain</option>
                   <option value="Sri Lanka">Sri Lanka</option>
                   <option value="Sudan">Sudan</option>
                   <option value="Suriname">Suriname</option>
                   <option value="Svalbard and Jan Mayen">Svalbard & Jan Mayen</option>
                   <option value="Swaziland">Eswatini</option>
                   <option value="Sweden">Sweden</option>
                   <option value="Switzerland">Switzerland</option>
                   <option value="Syrian Arab Republic">Syria</option>
                   <option value="Taiwan, Province of China">Taiwan</option>
                   <option value="Tajikistan">Tajikistan</option>
                   <option value="Tanzania, United Republic of">Tanzania</option>
                   <option value="Thailand">Thailand</option>
                   <option value="Timor-Leste">Timor-Leste</option>
                   <option value="Togo">Togo</option>
                   <option value="Tokelau">Tokelau</option>
                   <option value="Tonga">Tonga</option>
                   <option value="Trinidad and Tobago">Trinidad & Tobago</option>
                   <option value="Tunisia">Tunisia</option>
                   <option value="Turkey">Turkey</option>
                   <option value="Turkmenistan">Turkmenistan</option>
                   <option value="Turks and Caicos Islands">Turks & Caicos Islands</option>
                   <option value="Tuvalu">Tuvalu</option>
                   <option value="Uganda">Uganda</option>
                   <option value="Ukraine">Ukraine</option>
                   <option value="United Arab Emirates">United Arab Emirates</option>
                   <option value="United Kingdom">United Kingdom</option>
                   <option value="United States">United States</option>
                   <option value="United States Minor Outlying Islands">U.S. Outlying Islands</option>
                   <option value="Uruguay">Uruguay</option>
                   <option value="Uzbekistan">Uzbekistan</option>
                   <option value="Vanuatu">Vanuatu</option>
                   <option value="Venezuela">Venezuela</option>
                   <option value="Viet Nam">Vietnam</option>
                   <option value="Virgin Islands, British">British Virgin Islands</option>
                   <option value="Virgin Islands, U.s.">U.S. Virgin Islands</option>
                   <option value="Wallis and Futuna">Wallis & Futuna</option>
                   <option value="Western Sahara">Western Sahara</option>
                   <option value="Yemen">Yemen</option>
                   <option value="Zambia">Zambia</option>
                   <option value="Zimbabwe">Zimbabwe</option>
               </select>
            </div>
        </div>
        <div class="row mt-4">
           <div class="col-6">
               <label for="profession">Profession</label>
               <input type="text" class="form-control" id="companion_profession" name="companion[profession]">
           </div>
           <div class="col-6">
               <label for="organization">Organization</label>
               <input type="text" class="form-control" id="companion_organization" name="companion[organization]">
           </div>
       </div>
        <div class="row mt-4">
           <div class="col-6">
               <label for="visa_duration">Visa duration</label>
               <input type="text" class="form-control" id="companion_visa_duration" name="companion[visa_duration]">
           </div>
           <div class="col-6">
               <label for="visa_status">visa status</label>
               <select class="form-control" name="companion[visa_status]" id="companion_visa_status">
                   <option value="multiple">multiple</option>
                   <option value="single">single</option>
               </select>
           </div>
       </div>
        <div class="row mt-4">
           <div class="col-6">
               <label for="arrival_date">Arrival date</label>
               <input type="date" class="form-control" max="${my_date}" id="companion_arrival_date" name="companion[arrival_date]">
            </div>
           <div class="col-6">
               <label for="passport_photo">passport photo</label>
               <input type="file" accept="image/*" class="form-control" name="companion[passport_photo]" id="companion_passport_photo">
           </div>
       </div>
        <div class="row mt-4">
               <label for="personal_photo">Personal photo</label>
               <input type="file" accept="image/*" class="form-control" name="companion[personal_photo]" id="companion_personal_photo">
       </div>`);
    } else
        $('#companion_info').html('');
});

$('#accommodations_type').on('change', function () {
    let my_date = new Date().toISOString().substring(0, 10);
    let max_date = new Date();
    if (this.value == 'Eligible stay (5-nigh)') {
        max_date.setDate(max_date.getDate() + 4);
        max_date = new Date(max_date).toISOString().substring(0, 10);
        max_date = `max="${max_date}"`;
    }
    else {
        max_date = '';
    }


    $('#accommodation_info').html(`<div class="row mt-4">
        <div class="col-6">
            <label for="check_in_date">Check-in date</label>
            <input required type="date" class="form-control" ${max_date} min="${my_date}" id="check_in_date" name="check_in_date">
         </div>
        <div class="col-6">
        <label for="check_out_date">Check-out date</label>
        <input required type="date" class="form-control" min="${my_date}" id="check_out_date" name="check_out_date">
        </div>
    </div>`)
})
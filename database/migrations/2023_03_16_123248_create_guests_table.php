<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->string('email')->unique()->nullable();
            $table->date('issue_date');
            $table->date('expiry_date');
            $table->date('arrival_date');
            $table->string('place_of_birth');
            $table->string('place_of_issue');
            $table->string('country_of_residency');
            $table->string('gender');
            $table->string('passport_no');
            $table->string('profession')->nullable();
            $table->string('organization')->nullable();
            $table->string('visa_duration')->nullable();
            $table->string('visa_status');
            $table->text('passport_photo');
            $table->text('personal_photo');
            $table->unsignedBigInteger('companion_for_id')->nullable();
            $table->timestamps();

            $table->foreign('companion_for_id')->references('id')->on('guests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guests');
    }
};

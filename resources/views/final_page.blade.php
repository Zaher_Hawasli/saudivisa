@extends('CMS.Master')
@section('title')
 final-page
@stop
@section('Content')

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            {{-- <div class="col-xl-10 col-lg-12 col-md-9"> --}}
            <div class="col-xl-6 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg mt-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div>
                            {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
                            <div>
                                <div class="p-5">
                                    <div class="text-center">
                                        <h3 class="h4 text-gray-900 mb-4">
                                            Well Done 
                                            <p>
                                            Your information has been submitted successfully
                                            You will receive in coming day invitation email with instructions from RS4IT to book your flight.
                                            See you soon
                                            <p>
                                             </h3>
                                    </div>
                                    <hr>
                                    {{-- <div class="text-center">
                                        <a class="small" href="register.html">Create an Account!</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    @stop
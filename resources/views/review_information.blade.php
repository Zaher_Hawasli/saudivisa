@extends('CMS.Master')
@section('title')
review information
@stop
@section('Content')

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        {{-- <div class="col-xl-10 col-lg-12 col-md-9"> --}}
            <div class="col-xl-6 col-lg-12 col-md-9">

                <div class="card o-hidden guest_info border-0 shadow-lg mt-5">
                    <div class="card-body p-0 ">
                        <!-- Nested Row within Card Body -->
                        <div>
                            {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
                            <div>
                                <div class="p-3">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Guest Info</h1>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="first_name">First Name :</label>
                                            <div>{{$guest->first_name}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="last_name">Last Name :</label>
                                            <div>{{$guest->last_name}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="date_of_birth">Date of Birth :</label>
                                            <div>{{$guest->date_of_birth}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="gender">Gender :</label>
                                            <div>{{$guest->gender}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="place_of_birth">place of birth :</label>
                                            <div>{{$guest->place_of_birth}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="country_of_residency">Country of residency :</label>
                                            <div>{{$guest->country_of_residency}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="email">Email :</label>
                                            <div>{{$guest->email}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="passport_no">Passport No :</label>
                                            <div>{{$guest->passport_no}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="issue_date">Issue date :</label>
                                            <div>{{$guest->issue_date}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="expiry_date">Expiry date :</label>
                                            <div>{{$guest->expiry_date}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="place_of_issue">Place of issue :</label>
                                            <div>{{$guest->place_of_issue}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="profession">Profession :</label>
                                            <div>{{$guest->profession}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="organization">Organization :</label>
                                            <div>{{$guest->organization}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="visa_duration">Visa duration :</label>
                                            <div>{{$guest->visa_duration}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="visa_status">visa status :</label>
                                            <div>{{$guest->visa_status}}</div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-6">
                                            <label for="arrival_date">Arrival date :</label>
                                            <div>{{$guest->arrival_date}}</div>
                                        </div>
                                        <div class="col-6">
                                            <label for="passport_photo">passport photo :</label>
                                            <div>
                                                <img width="45px" height="45px" class="rounded" data-lity
                                                    style="cursor: pointer" src="{{$guest->passport_photo}}" alt="Img">
                                            </div>
                                        </div>
                                    </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="personal_photo">Personal photo :</label>
                                        <div>
                                            <img width="45px" height="45px" class="rounded" data-lity
                                                style="cursor: pointer" src="{{$guest->personal_photo}}" alt="Img">
                                        </div>
                                    </div>
                                </div>

                                @if(isset($guest->Guest))
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4 mt-3">Guest Companion Info</h1>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="first_name">First Name :</label>
                                        <div>{{$guest->Guest->first_name}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="last_name">Last Name :</label>
                                        <div>{{$guest->Guest->last_name}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="date_of_birth">Date of Birth :</label>
                                        <div>{{$guest->Guest->date_of_birth}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="gender">Gender :</label>
                                        <div>{{$guest->Guest->gender}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="place_of_birth">place of birth :</label>
                                        <div>{{$guest->Guest->place_of_birth}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="country_of_residency">Country of residency :</label>
                                        <div>{{$guest->Guest->country_of_residency}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="email">Email :</label>
                                        <div>{{$guest->Guest->email}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="passport_no">Passport No :</label>
                                        <div>{{$guest->Guest->passport_no}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="issue_date">Issue date :</label>
                                        <div>{{$guest->Guest->issue_date}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="expiry_date">Expiry date :</label>
                                        <div>{{$guest->Guest->expiry_date}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="place_of_issue">Place of issue :</label>
                                        <div>{{$guest->Guest->place_of_issue}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="profession">Profession :</label>
                                        <div>{{$guest->Guest->profession}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="organization">Organization :</label>
                                        <div>{{$guest->Guest->organization}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="visa_duration">Visa duration :</label>
                                        <div>{{$guest->Guest->visa_duration}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="visa_status">visa status :</label>
                                        <div>{{$guest->Guest->visa_status}}</div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <label for="arrival_date">Arrival date :</label>
                                        <div>{{$guest->Guest->arrival_date}}</div>
                                    </div>
                                    <div class="col-6">
                                        <label for="passport_photo">passport photo :</label>
                                        <div>
                                            <img width="45px" height="45px" class="rounded" data-lity
                                                style="cursor: pointer" src="{{$guest->Guest->passport_photo}}"
                                                alt="Img">
                                        </div>
                                    </div>
                                </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <label for="personal_photo">Personal photo :</label>
                                    <div>
                                        <img width="45px" height="45px" class="rounded" data-lity
                                            style="cursor: pointer" src="{{$guest->Guest->personal_photo}}" alt="Img">
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mt-3 mb-4">Guest Accommodations Info</h1>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <label for="accommodations_type">Accommodations type</label>
                                    <div>{{$guest->Accommodation[0]->accommodations_type}}</div>
                                </div>
                                <div class="col-6">
                                    <label for="room_type">Room type</label>
                                    <div>{{$guest->Accommodation[0]->room_type}}</div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <label for="check_in_date">Check-in date</label>
                                    <div>{{$guest->Accommodation[0]->check_in_date}}</div>
                                </div>
                                <div class="col-6">
                                    <label for="check_out_date">Check-out date</label>
                                    <div>{{$guest->Accommodation[0]->check_out_date}}</div>
                                </div>
                            </div>
                        </div>

                            <div class="row">
                                <div class="col-12 mt-4">
                                    <a href="/confirm" id="next_btn"
                                        class="btn btn-dark btn-user btn-block">Confirm</a>
                                </div>
                            </div>
                        </div>
                            {{-- <div class="text-center">
                                <a class="small" href="register.html">Create an Account!</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>
@stop
@section('script')
<script src="{{asset('js/CMS/Guest.js')}}"></script>
@stop
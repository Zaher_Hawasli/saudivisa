<!DOCTYPE html>
<html lang="en">

<head>

    @include('CMS.Include.Head')

</head>

<body id="page-top" class="overflow-hidden">
     <!-- Page Wrapper -->
     <div id="wrapper">
         @include('CMS.Include.Sidebar')
         <!-- Content Wrapper -->
        <div id="content-wrapper" class="overflow-auto d-flex flex-column content_wrapper">

            <!-- Main Content -->
            <div id="content">
                @include('CMS.Include.Nav')
                @yield('Content')
            </div>
            <!-- End of Main Content -->

           

        </div>
        <!-- End of Content Wrapper -->
     </div>
     @include('CMS.Include.Script')
     @yield('script')
</body>
</html>
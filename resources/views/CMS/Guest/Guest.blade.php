@extends('CMS.Master')
@section('title')
Guest
@stop
@section('Content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Guest</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
                <a href="/" class="btn btn-dark">
                    Add Guest
                    <i class="fas fa-plus"></i>
                </a>
    </div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Guest Table</h6>
    </div>
    <div class="card-body">
        <div id="Guest_table_div" >
            <table id="table" class="responsive table table-bordered" cellspacing="0">
                {{-- <table id="category_data_table" class="text-center table table-striped table-hover table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="table_info" style="width: 1006px;"> --}}
                <thead>
                    <tr>
                        <th>Photo</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Birth Date</th>
                        <th>FGender</th>
                        <th>Passport No</th>
                        <th>Visa Status</th>
                        <th>Companion</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
    </div>
    @include('CMS.Guest.Modals')
@stop
@section('script')
    <script src="{{asset('js/CMS/Guest.js')}}"></script>
@stop
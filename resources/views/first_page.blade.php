@extends('CMS.Master')
@section('title')
 Dashboard
@stop
@section('Content')

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            {{-- <div class="col-xl-10 col-lg-12 col-md-9"> --}}
            <div class="col-xl-6 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg mt-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div>
                            {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
                            <div>
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Mobile number verification </h1>
                                    </div>
                                    <form action="/second_page" method="post" class="user">
                                         @csrf
                                         <div class="form-group">
                                            <label for="mobileNumber">Please enter your mobile number</label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <select class="form-control" id="countryCode">
                                                        <option>Select Country Code</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="mobileNumber" placeholder="Mobile No (English Numbers only)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="otpCode">OTP CODE</label>
                                            <input type="text" class="form-control" id="otpCode" placeholder="Enter 4 digit OTP Code (English Characters only)" />
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-12 mb-3">
                                               <label for="email">Email</label>
                                               @error('email')
                                         <span class="alert-danger" role="alert">
                                             {{ $message }}
                                         </span>
                                     @enderror
                                               <input type="email" class="form-control" name="email" id="email">
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-dark btn-user btn-block" value="Next">
                                        {{-- <a href="index.html" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login with Google
                                        </a>
                                        <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                                        </a> --}}
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        @if($errors->any())
                                         @foreach ($errors as $error )
                                             <strong>$error</strong>
                                         @endforeach
                                         @endif
                                         @error('email')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                     @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        {{-- <a class="small" href="forgot-password.html">Forgot Password?</a> --}}
                                    </div>
                                    {{-- <div class="text-center">
                                        <a class="small" href="register.html">Create an Account!</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    @stop
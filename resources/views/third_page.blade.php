@extends('CMS.Master')
@section('title')
third_page
@stop
@section('Content')

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            {{-- <div class="col-xl-10 col-lg-12 col-md-9"> --}}
            <div class="col-xl-6 col-lg-12 col-md-9">

                <div class="card o-hidden guest_info border-0 shadow-lg mt-5">
                    <div class="card-body p-0 ">
                        <!-- Nested Row within Card Body -->
                        <div>
                            {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
                            <div>
                                <div class="p-3">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Add Guest Accommodations</h1>
                                    </div>
                                    <form enctype="multipart/form-data" id="add_accommodation_form" method="post" class="user">
                                         @csrf
                                         <div class="form-group">
                                             <div class="row mt-4">
                                                 <div class="col-6">
                                                    <label for="accommodations_type">Accommodations type</label>
                                                    <select required class="form-control" name="accommodations_type" id="accommodations_type">
                                                        <option value="" selected disabled>accommodations type</option>
                                                        <option value="Eligible stay (5-nigh)">Eligible stay (5-nigh)</option>
                                                        <option value="Extra night">Extra night</option>
                                                    </select>
                                                 </div>
                                                 <div class="col-6">
                                                    <label for="room_type">Room type</label>
                                                    <select required class="form-control" name="room_type" id="room_type">
                                                        <option value="King bed">King bed</option>
                                                        <option value="twin bed">twin bed</option>
                                                    </select>
                                                </div>
                                             </div>
                                          </div>
                                        <div id="accommodation_info">
                                            
                                         </div>
                                        </div>
                                         <div class="row">
                                             <div class="col-6">
                                                <button type="submit" value="save" class="btn btn-user btn-block btn-dark">Save</button>
                                             </div>
                                             <div class="col-6">
                                                <a href="/review_information" id="next_btn" class="btn disabled btn-dark btn-user btn-block">Next</a>
                                             </div>
                                         </div>
                                        {{-- <a href="index.html" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login with Google
                                        </a>
                                        <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                                        </a> --}}
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        @if($errors->any())
                                         @foreach ($errors as $error )
                                             <strong>$error</strong>
                                         @endforeach
                                         @endif
                                         @error('email')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                     @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        {{-- <a class="small" href="forgot-password.html">Forgot Password?</a> --}}
                                    </div>
                                    {{-- <div class="text-center">
                                        <a class="small" href="register.html">Create an Account!</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    @stop
    @section('script')
    <script src="{{asset('js/CMS/Guest.js')}}"></script>
@stop
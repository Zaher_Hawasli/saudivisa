<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\AccommodationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [FrontEndController::class,'index']);

Route::post('/second_page', [FrontEndController::class,'show_second_page']);

Route::get('/third_page', [FrontEndController::class,'show_third_page']);

Route::get('/review_information', [FrontEndController::class,'review_information']);

Route::get('/confirm', [FrontEndController::class,'Confirm']);

Route::get('/GuestTable', [GuestController::class,'index'])->name('GuestTable');

Route::post('/GuestDataTable', [GuestController::class,'Datatable']);

Route::resource('guest', GuestController::class);

Route::resource('accommodation', AccommodationController::class);




Route::get('/dashboard', function () {
    return view('CMS.Dashboard');
})->name('dashboard');


